﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LibNoise;
using LibNoise.Generator;
using LibNoise.Operator;

public class PerlinTexture : MonoBehaviour {

    public int sizeX = 256;
    public int sizeY = 256;
    
    [Range(0, 49)]
    public float scaleX;
    [Range(0, 49)]
    public float scaleY;

    public int offsetX = 100;
    public int offsetY = 100;

    public Texture2D texture;

    public void Start()
    {
    }

    public void Update()
    {
    }

    public Texture2D Generate()
    {
        Perlin perlin = new Perlin();
        ModuleBase moduleBase = perlin;

        Noise2D heigthMap = new Noise2D(sizeX, sizeY, moduleBase);
        float right = (float)(offsetX / (50 - scaleX));
        float left = (float)((offsetX + sizeX) / (50 - scaleX));
        float top = (float)(offsetY / (50 - scaleY));
        float bottom = (float)((sizeY + offsetY) / (50 - scaleY));

        heigthMap.GeneratePlanar(right,left,top,bottom);

        return texture = heigthMap.GetTexture(GradientPresets.Grayscale);
        
    }
}
