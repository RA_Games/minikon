﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Arrow : NetworkBehaviour {

    public int damage = 10;
    public int speed = 10;
    public bool hasCollided = false;
    public float force = 0;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Translate(Vector3.forward * Time.deltaTime * speed);
        CheckCollision();
	}

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            other.gameObject.GetComponent<Player>().GetDamage(damage);
            hasCollided = true;
            Destroy(this.gameObject);
        }
        /*if (other.gameObject.GetComponent<Tower>() == null)
        {
            Debug.Log("there");
            hasCollided = true;
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }*/

    public void CheckCollision()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.forward,out hit, 0.225f))
        {
            Debug.Log(hit.collider.gameObject.name);
            if(hit.collider.gameObject.GetComponentInParent<Player>()!=null)
            {

                var direction = new Vector3(hit.transform.position.x - this.transform.position.x, 0.0f, hit.transform.position.z - this.transform.position.z).normalized;
                hit.collider.gameObject.GetComponentInParent<Player>().GetDamage(damage, direction, force);
            }
            Destroy(gameObject);
        }
    }
}
