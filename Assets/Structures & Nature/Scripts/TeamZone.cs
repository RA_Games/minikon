﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TeamZone : NetworkBehaviour {

    public Teams team;
    [SyncVar(hook = "UpdateTeams")]
    public int gold;

    private Text _redTeam, _blueTeam, _yellowTeam, _purpleTeam;

    public Text RedText
    {
        get
        {
            if (_redTeam == null)
            {
                try
                {
                    _redTeam = GameObject.Find("Team_Red").GetComponentInChildren<Text>();
                }
                catch
                {
                    return null;
                }
            }

            return _redTeam;
        }
    }

    public Text BlueText
    {
        get
        {
            if (_blueTeam == null)
            {
                try
                {
                    _blueTeam = GameObject.Find("Team_Blue").GetComponentInChildren<Text>();
                }
                catch
                {
                    return null;
                }
            }

            return _blueTeam;
        }
    }

    public Text YellowText
    {
        get
        {
            if (_yellowTeam == null)
            {
                try
                {
                    _yellowTeam = GameObject.Find("Team_Yellow").GetComponentInChildren<Text>();
                }
                catch
                {
                    return null;
                }
            }

            return _yellowTeam;
        }
    }

    public Text PurpleText
    {
        get
        {
            if (_purpleTeam == null)
            {
                try
                {
                    _purpleTeam = GameObject.Find("Team_Purple").GetComponentInChildren<Text>();
                }
                catch
                {
                    return null;
                }
            }

            return _purpleTeam;
        }
    }

    void Start ()
    {
        gold = 0;
	}
	
    public void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponentInParent<Player>();

        if (player)
        {
            if(team.Equals(player.team))
            {
                AddGold(player.GetCoins());
            }
        }
    }

    public void AddGold(int gold)
    {
        this.gold += gold;
    }

    private void UpdateTeams(int value)
    {
        try
        {
            switch (team)
            {
                case Teams.BLUE: BlueText.text = gold.ToString(); break;
                case Teams.RED: RedText.text = gold.ToString(); break;
                case Teams.YELLOW: YellowText.text = gold.ToString(); break;
                case Teams.PURPLE: PurpleText.text = gold.ToString(); break;
            }
        }catch{}
    }
}
