﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class WorldObject : NetworkBehaviour, IDamagable, IDestructible
{
    [SyncVar]
    public int _life = 50;

    [SyncVar]
    public Status state;

    private Animator _anim;

    [Command]
    public void CmdDebug(string text)
    {
        RpcDebug(text);
    }

    [ClientRpc]
    public void RpcDebug(string text)
    {
        //print(text);
    }

    void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
    }

    public int Life
    {
        get
        {
            return _life;
        }
    }

    protected void SetMaxLife(int amount)
    {
        _life = amount;
    }

    public virtual void Destroy()
    {
        _anim.SetTrigger("Destroy");
        state = Status.DESTROYED;
    }
    
    [Command]
    private void CmdDestroy()
    {
        RpcDestroy();
    }

    [ClientRpc]
    private void RpcDestroy()
    {
        Destroy();
    }

    public virtual void GetDamage(int damage, Vector3 direction, float force)
    {
        if(state.Equals(Status.DESTROYED))
            return;

        _life -= damage;
        _anim.SetTrigger("TakeDamage");

        if (_life <= 0)
        {
            CmdDestroy();
        }
    }
}

public enum Status
{
    NORMAL,
    DESTROYED
}
