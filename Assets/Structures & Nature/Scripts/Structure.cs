﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.Networking;

public class Structure : WorldObject, ILooteable
{
    public Size _size = Size.SMALL;

    public Loot loot;
    private GameObject _reward;

    void Start()
    {
        SetMaxLife((int)_size * Life);
    }

    public GameObject Reward
    {
        get
        {
            if (_reward == null)
            {
                switch (loot)
                {
                    case Loot.COINS:
                        _reward = Resources.Load<GameObject>("Rewards/Coin");
                        break;
                    case Loot.FOOD:
                        _reward = Resources.Load<GameObject>("Rewards/Food");
                        break;
                    case Loot.POWERUPS:
                        _reward = Resources.Load<GameObject>("Rewards/PowerUp");
                        break;
                }
            }

            return _reward;

        }
    }

    public override void Destroy()
    {
        Droop();
        base.Destroy();
    }

    public void Droop()
    {
        CmdSpawnReward();
    }

    [Command]
    public void CmdSpawnReward()
    {
        for (int i = 0; i < ((int)_size * 3)+1; i++)
        {
            GameObject rw = Instantiate(Reward);
            rw.transform.position = new Vector3(this.transform.position.x + Random.Range(-5.0f, 5.0f), 10.0f, this.transform.position.z + Random.Range(-5.0f, 5.0f));
            NetworkServer.Spawn(rw);
        }

    }

}

[System.Flags]
public enum Loot
{
    COINS,
    FOOD,
    POWERUPS,
}

public enum Size
{
    SMALL = 3,
    MEDIUM = 5,
    BIG = 8
}

