﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Construction : NetworkBehaviour, IDamagable
{
    [SyncVar]
    public int currentHealth;
    public bool destroyed;
    public Animator anim;
    public bool flag = true;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    [ClientRpc]
    public void RpcDestroy()
    {
        CmdDestroy();
    }

    [Command]
    public void CmdDestroy()
    {
        Destroy();
    }

    public void GetDamage(int damage, Vector3 direction, float force)
    {
        currentHealth -= damage;
        anim.SetTrigger("TakeDamage");
        if (currentHealth <= 0)
        {
            CmdDestroy();
        }
    }

    public void Destroy()
    {
        anim.SetTrigger("Destruction");

        Destroy(gameObject);
    }
}
