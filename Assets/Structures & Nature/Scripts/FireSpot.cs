﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FireSpot : NetworkBehaviour {

    public List<Player> objectives;
    public GameObject arrow;
    public int coolDown;
    Vector3 target = new Vector3();
    float distance = Mathf.Infinity;
    public bool canAttack = true;
    public float width;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerStay(Collider other)
    {

        if (canAttack)
        {
            Attack(other.gameObject);
            /*if(isServer)
            {
                RpcAttack(other.gameObject);
            }*/
        }
    }

    [ClientRpc]
    public void RpcAttack(GameObject player)
    {
        CmdAttack(player);
    }

    [Command]
    public void CmdAttack(GameObject player)
    {
        Attack(player);
    }

    public void Attack(GameObject player)
    {
        target = player.transform.position;
        this.transform.LookAt(target);
        this.transform.position = this.transform.position + Vector3.forward * width;
        this.transform.LookAt(target);
        GameObject proyectile = Instantiate(arrow, this.transform.position, this.transform.rotation);
        canAttack = false;
        this.transform.position = this.transform.position - Vector3.forward * width;
        StartCoroutine(ReduceCooldown());
    }


    public bool CanSee(Vector3 viewer, GameObject objective)
    {
        if (objective == null)
            return false;
        RaycastHit hit;
        Ray ray = new Ray(viewer, objective.transform.position);

        if (Physics.Linecast(ray.origin, objective.transform.position, out hit))
        {
            if (hit.collider.gameObject.GetComponent<Player>() != null)
            {
                return true;
            }
        }
        return false;
    }

    IEnumerator ReduceCooldown()
    {
        yield return new WaitForSeconds(coolDown);
        canAttack = true;
    }
}
