﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nature : WorldObject {

    public override void GetDamage(int damage, Vector3 direction, float force)
    {
        base.GetDamage(damage, direction, force);

        CmdDebug("damaging: " + damage);
    }
}
