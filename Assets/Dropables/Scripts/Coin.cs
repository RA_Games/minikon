﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Coin : NetworkBehaviour
{
    public GameObject particle;

    private void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponentInParent<Player>();
        if (player != null)
        {
            player.coins += Random.Range(10,20) * 10;
            //CmdSpawnParticles();
            Destroy(this.gameObject);
            //coin particle
        }
    }

    [Command]
    private void CmdSpawnParticles()
    {
        GameObject go = Instantiate(particle,this.transform.position, Quaternion.identity);
        NetworkServer.Spawn(go);
    }


}
