﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {

    private static GameManager _singleton;

    private Player[] players;
    private Player myPlayer;

    private GameObject _spawn_red, _spawn_purple, _spawn_yellow, _spawn_blue;

    public static GameManager Manager
    {
        get
        {
            if (_singleton == null)
            {
                _singleton = FindObjectOfType<GameManager>();
            }

            return _singleton;
        }
    }

    public GameObject Spawn_Red
    {
        get
        {
            if (_spawn_red == null)
                _spawn_red = GameObject.Find("Platform_Red");

            return _spawn_red;
        }
    }

    public GameObject Spawn_Yellow
    {
        get
        {
            if (_spawn_yellow == null)
                _spawn_yellow = GameObject.Find("Platform_Yellow");

            return _spawn_yellow;
        }
    }

    public GameObject Spawn_Blue
    {
        get
        {
            if (_spawn_blue == null)
                _spawn_blue = GameObject.Find("Platform_Blue");

            return _spawn_blue;
        }
    }

    public GameObject Spawn_Purple
    {
        get
        {
            if (_spawn_purple == null)
                _spawn_purple = GameObject.Find("Platform_Purple");

            return _spawn_purple;
        }
    }

    public Vector3 Spawn_Red_Pos
    {
        get
        {
            if (_spawn_red == null)
                _spawn_red = GameObject.Find("Platform_Red");

            return new Vector3(_spawn_red.transform.position.x, _spawn_red.transform.position.y + 1, _spawn_red.transform.position.z);
        }
    }

    public Vector3 Spawn_Yellow_Pos
    {
        get
        {
            if (_spawn_yellow == null)
                _spawn_yellow = GameObject.Find("Platform_Yellow");

            return new Vector3(_spawn_yellow.transform.position.x, _spawn_yellow.transform.position.y + 1, _spawn_yellow.transform.position.z);
        }
    }

    public Vector3 Spawn_Blue_Pos
    {
        get
        {
            if (_spawn_blue == null)
                _spawn_blue = GameObject.Find("Platform_Blue");

            return new Vector3(_spawn_blue.transform.position.x, _spawn_blue.transform.position.y + 1, _spawn_blue.transform.position.z);
        }
    }

    public Vector3 Spawn_Purple_Pos
    {
        get
        {
            if (_spawn_purple == null)
                _spawn_purple = GameObject.Find("Platform_Purple");

            return new Vector3(_spawn_purple.transform.position.x, _spawn_purple.transform.position.y + 1, _spawn_purple.transform.position.z);
        }
    }

    public void SetPlayers(Player[] players)
    {
        this.players = players;

        foreach (Player p in players)
        {
            if (p.hasAuthority)
            {
                myPlayer = p;
                break;
            }
        }
    }

    public Vector3 GetRespawnPoint()
    {
        switch (myPlayer.team)
        {
            case Teams.BLUE: return Spawn_Blue_Pos;
            case Teams.RED: return Spawn_Red_Pos;
            case Teams.YELLOW: return Spawn_Yellow_Pos;
            case Teams.PURPLE: return Spawn_Purple_Pos;
            default: return new Vector3(0,1,0);
        }
    }
    
    [Command]
    public void CmdFirstSpawn()
    {
        foreach (Player p in players)
        {
            switch (p.team)
            {
                case Teams.BLUE:
                    p.RpcInit(Spawn_Blue_Pos);
                    break;
                case Teams.RED:
                    p.RpcInit(Spawn_Red_Pos);
                    break;
                case Teams.YELLOW:
                    p.RpcInit(Spawn_Yellow_Pos);
                    break;
                case Teams.PURPLE:
                    p.RpcInit(Spawn_Purple_Pos);
                    break;
            }
        }
    }
}
