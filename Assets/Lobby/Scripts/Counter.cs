﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Counter : NetworkBehaviour {

    [SyncVar(hook = "UpdateCounter")]
    public string counter;
    public GameObject lobby;
    public bool fade;

    private Text _counter_text;
    private Texture2D blk;
    private float alph;

    private Text Counter_text
    {
        get
        {
            if (_counter_text == null)
                _counter_text = GetComponent<Text>();

            return _counter_text;
        }
    }

    private void Awake()
    {
        blk = new Texture2D(1, 1);
        blk.SetPixel(0, 0, new Color(1, 1, 1, 0));
        blk.Apply();
    }

    private void Update()
    {
        if (!fade)
        {
            if (alph > 0)
            {
                alph -= Time.deltaTime * .8f;
                if (alph < 0) { alph = 0f; }
                blk.SetPixel(0, 0, new Color(1, 1, 1, alph));
                blk.Apply();
            }
        }

        if (fade)
        {
            if (alph < 1)
            {
                alph += Time.deltaTime * .8f;
                if (alph > 1) { alph = 1f; }
                blk.SetPixel(0, 0, new Color(1, 1, 1, alph));
                blk.Apply();
            }
        }
    }

    private void UpdateCounter(string c)
    {
        if(c.Equals("GO!"))
        {
            fade = true;
        }

        if (c.Equals("OFF"))
        {
            fade = false;
            Init(FindObjectsOfType<Player>());
            Counter_text.text = string.Empty;
            return;
        }

        Counter_text.text = c;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blk);
    }

    public  void Init(Player[] players)
    {
        foreach (Player p in players)
        {
            p.gameObject.SetActive(false);
        }

        lobby.SetActive(false);
        counter = string.Empty;
        Camera.main.orthographicSize = 100;
        Camera.main.transform.position = new Vector3(0, 1, 0);
    }
}
