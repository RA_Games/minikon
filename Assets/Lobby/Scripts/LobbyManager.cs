﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyManager : NetworkBehaviour {

    public Counter counter;

    public MapEditor mapEditor;

    [SyncVar]
    bool ready = false;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        UpdatePlayerList();
    }

    void Update ()
    {
        foreach (Player p in UpdatePlayerList())
        {
            if (p.team.Equals(Teams.TEAMLESS))
            {
                StopAllCoroutines();
                counter.counter = string.Empty;
                ready = false;
                return;
            }
        }

        if (!ready)
            StartCoroutine(StartingMatch());
	}

    private Player[] UpdatePlayerList()
    {
        return GameObject.FindObjectsOfType<Player>();
    }

    private IEnumerator StartingMatch()
    {
        ready = true;

        yield return new WaitForSeconds(2);

        int seconds = 6;

        while (seconds > 1)
        {
            if(ready)
            {
                seconds--;
                counter.counter = seconds.ToString();
                yield return new WaitForSeconds(1);
            }
            else
            {
                yield break;
            }
        }

        counter.counter = "GO!";
        GameManager.Manager.SetPlayers(UpdatePlayerList());

        yield return new WaitForSeconds(1);

        counter.counter = "OFF";

        StartCoroutine(mapEditor.Init());

        yield return new WaitForSeconds(30);

        gameObject.SetActive(false);
    }
}
