﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable {

    void GetDamage(int damage,Vector3 direction,float force);
   
}
