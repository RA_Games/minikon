﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruction : MonoBehaviour {

	void Start () {
        StartCoroutine(Destroy());
	}
	
	void Update () {
		
	}

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
}
