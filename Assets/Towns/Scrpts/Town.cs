﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RaycastExtensions;
using UnityEngine.Networking;

public class Town : NetworkBehaviour {

    public static List<Vector3> houses = new List<Vector3>();
    public bool townReady;

    private static GameObject[] _houses_prefabs;
    private int widht, height;

    private int townSize;
    private int housesAmount;

    public static bool GetTownReady()
    {
        foreach (Town t in FindObjectsOfType<Town>())
        {
            if (t.townReady.Equals(false))
                return false;
        }

        return true;
    }

    public static GameObject[] HousePrefabs
    {
        get
        {
            if(_houses_prefabs == null)
                _houses_prefabs = Resources.LoadAll<GameObject>("Map/Houses");

            return _houses_prefabs;
        }
    }

    public void Init ()
    {
        SetTownSize();
        StartCoroutine(GenerateVillage());
	}

    private IEnumerator GenerateVillage()
    {
        for (int i = 0; i < housesAmount; i++)
        {
            Vector3 pos = RandomPoints.SendRayDown(townSize, transform.position);

            if (RandomPoints.ValidateDistance(pos, houses, 15))
            {
                if (Random.Range(0f, 1) > Vector3.Distance(pos, Vector3.zero) / ((float)MapEditor.Editor.mapSize / 2))
                {
                    CreateHouse(pos).transform.SetParent(transform);
                    yield return new WaitForSeconds(Time.deltaTime);
                }
            }
        }
        townReady = true;
    }

    private GameObject CreateHouse(Vector3 position)
    {
        GameObject house = Instantiate(GetHousePrefab());
        house.transform.position = position;
        house.transform.Rotate(0, Random.Range(0, 360), 0);
        houses.Add(position);

        NetworkServer.Spawn(house);

        return house;
    }


    private GameObject GetHousePrefab()
    {
        GameObject aux = null;

        bool ready = false;

        do
        {
            aux = HousePrefabs[Random.Range(0, HousePrefabs.Length)];

            if(aux.name == "House_5")
            {
                if (Random.Range(0f, 1f) > 0.9f)
                {
                    ready = true;
                }
            }
            else
            {
                ready = true;
            }

        } while (!ready);

        return aux;
    }

    private void SetTownSize()
    {
        housesAmount = Random.Range(MapEditor.Editor.minHousesAmount, MapEditor.Editor.maxHousesAmount);
        townSize = Random.Range(MapEditor.Editor.minTownSize, MapEditor.Editor.maxTownSize);
    }
}
