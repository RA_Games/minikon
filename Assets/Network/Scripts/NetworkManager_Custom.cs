﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NetworkManager_Custom : NetworkManager {

	public GameObject[] registerPrefabs;

	public void StartupHost () 
	{
		SetPort ();

		NetworkManager.singleton.StartHost ();
	}
	
	public void JoinGame () 
	{
		SetIpAdress ();
		SetPort ();

		NetworkManager.singleton.StartClient ();

		RegisterPrefabs ();
	}

	void SetIpAdress ()
	{
		string ipAdress = GameObject.Find ("InputFieldIpAdress").transform.Find ("Text").GetComponent<Text>().text;
		NetworkManager.singleton.networkAddress = ipAdress;
	}

	void SetPort ()
	{
		NetworkManager.singleton.networkPort = 7777;
	}

	void RegisterPrefabs()
	{
		foreach(GameObject prefab in registerPrefabs)
		{
			ClientScene.RegisterPrefab (prefab);
		}
	}

    public void ChangeGameScene(string newScene)
    {
        onlineScene = newScene;
    }
}
