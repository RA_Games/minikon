﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public void StartHost()
    {
        NetworkManager.singleton.StartHost();
    }

    public void ChangeGameScene(string newScene)
    {
        NetworkManager.singleton.onlineScene = newScene;
    }

    void SetIpAdress()
    {
        string ipAdress = GameObject.Find("InputFieldIpAdress").transform.Find("Text").GetComponent<Text>().text;
        NetworkManager.singleton.networkAddress = ipAdress;
    }

    void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
    }

    public void JoinGame()
    {
        SetIpAdress();
        NetworkManager.singleton.StartClient();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
