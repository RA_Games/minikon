﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tutorial : NetworkBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        Destroy(NetworkManager.singleton.gameObject);
        NetworkManager.Shutdown();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Lobby");
    }
}
