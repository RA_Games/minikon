﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RaycastExtensions;
using UnityEngine.Networking;

public class MapEditor : NetworkBehaviour {

    [Header("Game settings")]
    public Difficult difficult = Difficult.EASY;
    public MapSize mapSize = MapSize.SMALL;

    [Space]

    [Header("Town configuration")]
    [Range(1, 50)]
    public int minTownSize = 50;
    [Range(20, 200)]
    public int maxTownSize = 100;
    [Range(1, 30)]
    public int minHousesAmount = 20;
    [Range(15, 100)]
    public int maxHousesAmount = 50;

    [Space]

    [Header("Decoration texture")]
    [Tooltip("256 x 256")]
    public Texture2D texture_small;
    [Tooltip("512 x 512")]
    public Texture2D texture_medium;
    [Tooltip("1024 x 1024")]
    public Texture2D texture_big;
    [Tooltip("2048 x 2048")]
    public Texture2D texture_giantic;

    private GameObject[] decorations_prefabs;

    private List<Vector3> townsToGenerate = new List<Vector3>();

    private int widht, height;
    private GameObject map;
    private Mesh mesh;

    GameObject towns;
    GameObject decorations;

    private GameObject _town_prefab;
    private MapData _mapData;
    private static MapEditor _singleton;

    public static MapEditor Editor
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<MapEditor>();

            return _singleton;
        }
    }

    public MapData MapData
    {
        get
        {
            if (_mapData == null)
                _mapData = new MapData();
            return _mapData;
        }
    }

    private void Awake()
    {
        towns = new GameObject("Towns");
        decorations = new GameObject("Decorations");

        _town_prefab = Resources.Load<GameObject>("Map/Town");
        decorations_prefabs = Resources.LoadAll<GameObject>("Map/Decorations");
    }

    public IEnumerator Init()
    {
        Town.houses.Clear();
        SetMap();

        yield return new WaitForSeconds(2);

        GenerateTownPlots();

        yield return new WaitForSeconds(3);

        while (!Town.GetTownReady())
        {
            yield return new WaitForSeconds(1);
        }

        StartCoroutine(GenerateDecorations());
    }

    private void SetMap()
    {
        map = Instantiate(Resources.Load<GameObject>("Map/Sizes/Map_" + mapSize));
        map.name = "Map";

        transform.localScale = map.transform.localScale;
        widht = (int)transform.localScale.x;
        height = (int)transform.localScale.z;

        NetworkServer.Spawn(map);
    }

    private void GenerateTownPlots()
    {
        for (int i = 0; i < (int)difficult; i++)
        {
            bool success = false;

            while (success.Equals(false))
            {
                Vector3 pos = RandomPoints.SendRayDown(widht,transform.position);

                if (RandomPoints.ValidateDistance(pos, MapData.mapObjects,(int) 20))
                {
                    StoreTownPlot(pos);
                    success = true;
                }
            }
        }

        StartCoroutine(CreateTownPlots());
    }

    private void StoreTownPlot(Vector3 plot)
    {
        townsToGenerate.Add(plot);
    }

    private IEnumerator CreateTownPlots()
    {
        foreach (Vector3 v in townsToGenerate)
        {
            GameObject townPlot = Instantiate(_town_prefab);
            townPlot.transform.position = v;
            townPlot.transform.SetParent(towns.transform);

            townPlot.GetComponent<Town>().Init();
            MapData.mapObjects.Add(v);

            yield return new WaitForSeconds(Time.deltaTime / 5);
        }
    }

    private Texture2D GetTextureOfMap()
    {
        switch (mapSize)
        {
            case MapSize.SMALL: return texture_small;
            case MapSize.MEDIUM: return texture_medium;
            case MapSize.BIG: return texture_big;
            case MapSize.GIANTIC: return texture_giantic;
            default: return texture_small;
        }
    }

    private IEnumerator GenerateDecorations()
    {
        if (GetTextureOfMap() == null)
            yield return null;

        for (int x = 10; x < GetTextureOfMap().width - 10; x++)
        {
            for (int y = 5; y < GetTextureOfMap().height - 5; y++)
            {
                if (GetTextureOfMap().GetPixel(x,y).grayscale > 0.3f)
                {
                    Vector3 pos = new Vector3(-widht / 2, 0, -height / 2);
                    pos.x += x;
                    pos.z += y;
                    pos.y += 0.5f;

                    Vector3 seed = RandomPoints.SendRayDown(widht, height, pos);

                    if (RandomPoints.ValidateDistance(pos, Town.houses, 10))
                    {
                        if (RandomPoints.ValidateDistance(pos, MapData.mapObjects, 5))
                        {
                            GameObject deco = Instantiate(decorations_prefabs[Random.Range(0, decorations_prefabs.Length - 3)]);
                            deco.transform.position = pos;
                            deco.transform.SetParent(decorations.transform);
                            MapData.mapObjects.Add(pos);
                            NetworkServer.Spawn(deco);
                        }
                    }
                }
            }

            yield return new WaitForSeconds(Time.deltaTime);
        }

        GameManager.Manager.CmdFirstSpawn();
    }
}

public enum Difficult
{
    EASY = 12,
    NORMAL = 9,
    HARD = 6,
    INSANE = 3
}

public enum MapSize
{
    SMALL = 256,
    MEDIUM = 512,
    BIG = 1024,
    GIANTIC = 2048
}

public class MapData
{
    public List<Vector3> mapObjects = new List<Vector3>();
}

