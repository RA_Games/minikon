﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamSelector : MonoBehaviour {
    
    public Teams team;
    public List<Player> players;
    
    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponentInParent<Player>();

        if (other != null)
        {
            p.ChangeTeam(team);
        }

        players.Add(p);
    }

    private void OnTriggerExit(Collider other)
    {
        Player p = other.GetComponentInParent<Player>();

        if (other != null)
        {
            p.ChangeTeam(team);
        }

        players.Remove(p);
    }


}
