﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    private float coolDown = 0.5f; 
    private int damage = 20;
    private float force = 7000;
    public bool realized = false;
    private BoxCollider _boxCollider;

    private BoxCollider BoxCol
    {
        get
        {
            if (_boxCollider == null)
                _boxCollider = this.gameObject.GetComponent<BoxCollider>();

            return _boxCollider;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!realized) { 
            var objetive = other.gameObject.GetComponentInParent<IDamagable>();

            if (objetive != null)
            {
                var direction = new Vector3(other.gameObject.transform.position.x - this.transform.position.x,0.0f, other.gameObject.transform.position.z - this.transform.position.z).normalized;
                objetive.GetDamage(damage, direction ,force);
                realized = true;
            }
        }
    }

}
