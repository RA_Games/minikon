﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player : NetworkBehaviour , IDamagable
{
    [SyncVar(hook = "ChangeLife")]
    public int life;
    private int maxLife = 100;
    private float speed = 250;
    [SyncVar(hook = "ChangeTeam")]
    public Teams team = Teams.TEAMLESS;
    [SyncVar]
    public State state = State.STILL;
    private bool attack = true, defend = true;
    private float AttackCD = 0.7f, DefendCD = 3.0f;
    [SyncVar(hook = "ChangeCoins")]
    public int coins = 0;
    private float timeRespawn;
     
    private GameObject teamZone;
    private RectTransform teamZone_icon;
    RectTransform canvasRect;
    private bool ready;

    public Renderer helmet;
    public Renderer weapon;
    public Renderer shield;

    private GameObject model;

    private float deltaMovement = 0.75f;

    private Rigidbody rb;

    private Camera cam;
    private int offSetX = 4, offSetZ = 2;
    private Vector3 mousePosition;
    private Animator anim;

    private float maxDistanceHeigth = 10;

    public Image lifeImage;
    public Canvas canvas;
    public Text coinText;

    public GameObject bags;

    private void Awake()
    {
        coinText.text = "0";
    }

    private void Start() {
        if (hasAuthority)
        {
            life = maxLife;
            coins = 0;
            rb = GetComponent<Rigidbody>();
            cam = Camera.main;
            anim = GetComponent<Animator>();
            transform.Translate(Vector3.up);
        }
    }

    private void Update()
    {
        if (!hasAuthority)
            return;

        if (ready == false)
            return;

        if(teamZone == null)
        {
            try
            {
                switch (team)
                {
                    case Teams.BLUE:
                        teamZone = GameObject.Find("Platform_Blue");
                        break;
                    case Teams.RED:
                        teamZone = GameObject.Find("Platform_Red");
                        break;
                    case Teams.YELLOW:
                        teamZone = GameObject.Find("Platform_Yellow");
                        break;
                    case Teams.PURPLE:
                        teamZone = GameObject.Find("Platform_Purple");
                        break;
                }
            }
            catch
            {
                return;
            }
        }

        if (teamZone_icon == null)
        {
            try
            {
                teamZone_icon = GameObject.Find("TeamZone_icon").GetComponent<RectTransform>();
                canvasRect = teamZone_icon.GetComponentInParent<RectTransform>();
            }
            catch
            {
                return;
            }
        }

        Vector2 icon = Camera.main.WorldToScreenPoint(teamZone.transform.position);

        icon.x = Mathf.Clamp(icon.x, (teamZone_icon.sizeDelta.x / 2), Screen.width - (teamZone_icon.sizeDelta.x / 2));
        icon.y = Mathf.Clamp(icon.y, (teamZone_icon.sizeDelta.y / 2), Screen.height - (teamZone_icon.sizeDelta.y / 2));

        teamZone_icon.position = icon;
    }

    [ClientRpc]
    public void RpcInit(Vector3 pos)
    {
        transform.position = pos;
        gameObject.SetActive(true);
        ready = true;
    }

    public int GetCoins()
    {
        int aux = coins;
        coins = 0;

        return aux;
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
    }

    void LateUpdate()
    {
        if (!hasAuthority)
            return;

        Vector3 hitPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        cam.transform.position = Vector3.Lerp(cam.transform.position, new Vector3(((transform.position.x + hitPoint.x) / 2) + 5, cam.transform.position.y, ((transform.position.z + hitPoint.z) / 2) + 2), Time.deltaTime * 4);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 7, Time.deltaTime);
        CanvasRotation();
    }

    void FixedUpdate() {

        if (!hasAuthority)
            return;

        if (state != State.DEATH)
        {
            Movement(true);

            if (Input.GetMouseButton(0) && attack)
            {
                StartCoroutine(Attack());
            }
            else if (Input.GetMouseButton(1) && defend)
            {
                StartCoroutine(Defend());
            }

            /*second option
            if(state == State.ATTACK || state == State.DEFEND)
            {
                Rotation();
            }
            */
        }
    }

    public void CanvasRotation()
    {
        canvas.transform.rotation = new Quaternion(0, 90 - this.transform.rotation.y, 0, 360);
    }

    private void Movement()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 fVelocity = new Vector3(horizontal, 0, vertical).normalized * speed * Time.deltaTime;
        rb.velocity = fVelocity;

        Quaternion dirQ = Quaternion.LookRotation(rb.velocity) * Quaternion.Inverse(Quaternion.Euler(0,90,0));
        Quaternion slerp = Quaternion.Slerp(transform.rotation, dirQ, rb.velocity.magnitude * 50 * Time.deltaTime);
        rb.MoveRotation(slerp);

        anim.SetFloat("Speed", rb.velocity.magnitude);
    }

    //optional
    private void Movement(bool a)
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 fVelocity = new Vector3(vertical, 0, -horizontal).normalized * speed * Time.deltaTime;
        rb.velocity = fVelocity;
        anim.SetFloat("Speed", rb.velocity.magnitude);

        Rotation();
    }

    private void Rotation()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitdist = 0.0f;

        if (playerPlane.Raycast(ray, out hitdist))
        {

            Vector3 targetPoint = ray.GetPoint(hitdist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position ) * Quaternion.Inverse(Quaternion.Euler(0, 90, 0));
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
        }
    }

    private bool OutOfMap()
    {
        if (transform.position.y >= maxDistanceHeigth && maxDistanceHeigth >= transform.position.y )
        {
            Death();
        }
        return true;

    }

    private IEnumerator Attack()
    {
        state = State.ATTACK;
        attack = false;
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(AttackCD);
        attack = true;
        state = State.STILL;
    }

    private IEnumerator Defend()
    {
        Debug.Log("defendiendo");
        state = State.DEFEND;
        defend = false;
        anim.SetTrigger("Defend");
        yield return new WaitForSeconds(DefendCD);
        defend = true;
        state = State.STILL;

    } 

    public void GetDamage(int damage, Vector3 direction,float force)
    {
        rb.velocity = direction*force;
        //rb.AddForce(direction * force);
        life -= damage;
        if(life <= 0)
        {
            Death();
        }
    }

    public void Heal(int heal)
    {
        life = Mathf.Clamp( life + heal,0,maxLife);
    }

    
    private void Death()
    {
        CmdCoinsSpawn(coins);
        coins = 0;
        transform.position = GameManager.Manager.GetRespawnPoint();

        //state = State.DEATH;
        //rb.useGravity = false;
        //this.GetComponent<BoxCollider>().enabled = false;
        //model.SetActive(false);
    }

    [Command]
    private void CmdCoinsSpawn(int coins)
    {
        int aux = coins / 200;
        for(int i = 0;i<aux;i++)
        {
            GameObject b = Instantiate(bags,new Vector3(Random.Range(-3.0f,3.0f),0.0f, Random.Range(-3.0f, 3.0f)) +this.transform.position,Quaternion.identity);
            NetworkServer.Spawn(b);
        }
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(timeRespawn);
        life = maxLife;
        state = State.STILL;
        rb.useGravity = true;
        this.GetComponent<BoxCollider>().enabled = true;
        model.SetActive(true);
    }

    public void WeaponCD()
    {
        GetComponentInChildren<Weapon>().realized = false;
    }

    public void ChangeLife(int life)
    {
        lifeImage.fillAmount = (life * 1.0f) / (maxLife * 1.0f);
    }

    public void ChangeCoins(int coins)
    {
        coinText.text = coins.ToString();
    }

    public void ChangeTeam(Teams newTeam)
    {
        this.team = newTeam;

        Material helmet_mat = helmet.materials[2];
        Material weapon_mat = weapon.materials[2];
        Material shield_mat = shield.materials[1];


        switch (this.team)
        {
            case Teams.RED:
                helmet_mat.color = Color.red;
                weapon_mat.color = Color.red;
                shield_mat.color = Color.red;
                break;
            case Teams.BLUE:
                helmet_mat.color = Color.blue;
                weapon_mat.color = Color.blue;
                shield_mat.color = Color.blue;
                break;
            case Teams.PURPLE:
                helmet_mat.color = Color.magenta;
                weapon_mat.color = Color.magenta;
                shield_mat.color = Color.magenta;
                break;
            case Teams.YELLOW:
                helmet_mat.color = Color.yellow;
                weapon_mat.color = Color.yellow;
                shield_mat.color = Color.yellow;
                break;
            case Teams.TEAMLESS:
                helmet_mat.color = Color.gray;
                weapon_mat.color = Color.gray;
                shield_mat.color = Color.gray;
                break;
        }
    }
}

public enum State
{
    DEATH,
    WALK,
    RUN,
    ATTACK,
    DEFEND,
    STILL,
}

public enum Teams
{
    TEAMLESS,
    RED,
    BLUE,
    YELLOW,
    PURPLE
}