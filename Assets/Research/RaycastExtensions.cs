﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RaycastExtensions{

    public static class RandomPoints
    {
        public static Vector3 SendRayDown(int widht, int height, Vector3 center)
        {
            RaycastHit hit;

            int x = ((int)center.x) + (int)Random.Range(-(widht / 2), (widht / 2));
            int y = ((int)center.z) + (int)Random.Range(-(height / 2), (height / 2));

            Vector3 origin = new Vector3(x, center.y + 10, y);
            Vector3 direction = new Vector3(x, center.y - 5, y);

            if (Physics.Linecast(origin, direction, out hit))
            {
                return hit.point;
            }

            return center;
        }

        public static string GetHitTag(Vector3 center)
        {
            RaycastHit hit;

            Vector2 point = Vector2.zero;
            Vector2 centerPlanned = new Vector2(center.x, center.z);

            Vector3 origin = new Vector3(point.x, center.y + 10, point.y);
            Vector3 direction = new Vector3(point.x, center.y - 5, point.y);

            if (Physics.Linecast(origin, direction, out hit))
            {
                return hit.collider.tag;
            }


            return string.Empty;
        }

        public static Vector3 SendRayDown(int radius, Vector3 center)
        {
            RaycastHit hit;

            Vector2 point = Vector2.zero;
            Vector2 centerPlanned = new Vector2(center.x,center.z);
            float distance = 0;

            do
            {
                point.x = ((int)center.x) + (int)Random.Range(-radius, radius);
                point.y = ((int)center.z) + (int)Random.Range(-radius, radius);

                distance = Vector2.Distance(point, centerPlanned);

            } while (distance > radius);

            Vector3 origin = new Vector3(point.x, center.y + 10, point.y);
            Vector3 direction = new Vector3(point.x, center.y - 5, point.y);

            if (Physics.Linecast(origin, direction, out hit))
            {
                return hit.point;
            }

            return center;
        }

        public static bool ValidateDistance(Vector3 point, List<Vector3> points, int separation)
        {
            foreach (Vector3 p in points)
            {
                Vector2 pToPlannar = new Vector2(p.x, p.z);
                Vector2 pointToPlannar = new Vector2(point.x, point.z);

                if (Vector2.Distance(pToPlannar, pointToPlannar) < separation)
                {
                    return false;
                }
            }

            return true;
        }
    }

}
